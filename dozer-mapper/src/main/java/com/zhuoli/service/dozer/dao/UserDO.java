package com.zhuoli.service.dozer.dao;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

/**
 * @Author: zhuoli
 * @Date: 2018/9/30 16:19
 * @Description:
 */
@Getter
@Setter
@ToString
public class UserDO {
    private Integer id;
    private String name;
    private Integer age;
    private String gender;
    private String address;
    private String password;
    private String nickName;
    private Date birthday;
    private Integer isDeleted;
}
