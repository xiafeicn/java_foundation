package com.zhuoli.service.dozer.demo;

import com.zhuoli.service.dozer.dao.UserDO;
import com.zhuoli.service.dozer.util.BeanMapper;
import com.zhuoli.service.dozer.vo.UserVO;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;


/**
 * @Author: zhuoli
 * @Date: 2018/9/30 16:26
 * @Description:
 */
public class TransformDemo {
    private UserDO userDO = new UserDO();

    @Before
    public void init(){
        userDO.setId(1);
        userDO.setAge(18);
        userDO.setName("zhuoli");
        userDO.setAddress("shanghai");
        userDO.setBirthday(Date.from(LocalDate.of(2000, 1, 1).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));
        userDO.setGender("male");
        userDO.setNickName("卓立");
        userDO.setPassword("123456");
        userDO.setIsDeleted(0);
    }

    @Test
    public void transformInCommonWay(){
        UserVO userVO = new UserVO();
        userVO.setId(userDO.getId());
        userVO.setAge(userDO.getAge());
        userVO.setName(userDO.getName());
        userVO.setAddress(userDO.getAddress());
        userVO.setBirthday(userDO.getBirthday());
        userVO.setGender(userDO.getGender());
        userVO.setNickName(userDO.getNickName());
        userVO.setClickCount(99);

        System.out.println(userVO);
    }

    @Test
    public void transformInDozerWay(){
        UserVO userVO = BeanMapper.map(userDO, UserVO.class);
        userVO.setClickCount(99);
        System.out.println(userVO);
    }
}
