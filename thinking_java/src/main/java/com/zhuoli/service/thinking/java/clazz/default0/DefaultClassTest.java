package com.zhuoli.service.thinking.java.clazz.default0;

import org.junit.Test;

/**
 * @Author: zhuoli
 * @Date: 2018/10/18 21:54
 * @Description:
 */
public class DefaultClassTest {
    DefaultClass defaultClass = new DefaultClass();
    //同包中可见
    private DefaultClass.InnerDefaultClass innerDefaultClass = defaultClass.new InnerDefaultClass();

    @Test
    public void test(){
        innerDefaultClass.setInnerName("inner class");
        System.out.println(innerDefaultClass.getInnerName());

        //包访问权限,父类实例可以在同包下调用自己的protected方法
        defaultClass.print();
    }
}
