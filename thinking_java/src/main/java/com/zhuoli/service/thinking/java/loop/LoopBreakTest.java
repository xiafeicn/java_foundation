package com.zhuoli.service.thinking.java.loop;

import org.junit.Test;

/**
 * @Author: zhuoli
 * @Date: 2018/10/17 11:11
 * @Description:
 */
public class LoopBreakTest {

    @Test
    public void breakTest(){
        for (int i = 0; i < 3; i++) {
            System.out.println(String.format("start outer for loop index %d", i));
            for (int k = 0; k < 3; k++) {
                if (k == 1){
                    break;
                }
                System.out.println(String.format("inner loop index %d", k));
            }
            System.out.println(String.format("end outer for loop index %d", i));
        }
        System.out.println("loop end");
    }

    @Test
    public void breakLabelTest(){
        BREAK_LABEL:
        for (int i = 0; i < 3; i++) {
            System.out.println(String.format("start outer for loop index %d", i));
            for (int k = 0; k < 3; k++) {
                if (k == 1){
                    break BREAK_LABEL;
                }
                System.out.println(String.format("inner loop index %d", k));
            }
            System.out.println(String.format("end outer for loop index %d", i));
        }
        System.out.println("loop end");
    }
}
