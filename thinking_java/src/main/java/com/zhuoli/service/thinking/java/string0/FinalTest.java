package com.zhuoli.service.thinking.java.string0;

/**
 * @Author: zhuoli
 * @Date: 2018/10/31 21:35
 * @Description:
 */
public class FinalTest {
    public static void main(String[] args) {
        final int[] a1 = {1, 2, 3};
        a1[1] = 4; //这时候a1数组时{1, 4, 3}
    }
}
