package com.zhuoli.service.thinking.java.loop;

import org.junit.Test;

/**
 * @Author: zhuoli
 * @Date: 2018/10/17 11:19
 * @Description:
 */
public class LoopContinueTest {
    @Test
    public void continueTest(){
        for (int i = 0; i < 3; i++) {
            System.out.println(String.format("start outer for loop index %d", i));
            for (int k = 0; k < 3; k++) {
                if (k == 1){
                    continue;
                }
                System.out.println(String.format("inner loop index %d", k));
            }
            System.out.println(String.format("end outer for loop index %d", i));
        }
        System.out.println("loop end");
    }

    @Test
    public void continueLabelTest(){
        CONTINUE_LABEL:
        for (int i = 0; i < 3; i++) {
            System.out.println(String.format("start outer for loop index %d", i));
            for (int k = 0; k < 3; k++) {
                if (k == 1){
                    continue CONTINUE_LABEL;
                }
                System.out.println(String.format("inner loop index %d", k));
            }
            System.out.println(String.format("end outer for loop index %d", i));
        }
        System.out.println("loop end");
    }
}
