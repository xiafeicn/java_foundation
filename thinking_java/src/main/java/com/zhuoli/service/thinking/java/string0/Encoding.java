package com.zhuoli.service.thinking.java.string0;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.junit.Test;

import java.nio.charset.StandardCharsets;

/**
 * @Author: zhuoli
 * @Date: 2018/10/26 11:19
 * @Description:
 */
public class Encoding {

    public static void main(String[] args) {
        System.out.println(System.getProperty("file.encoding"));
        String str = new String(new int[]{150370}, 0, 1);
        System.out.println(str);

        String str1 = "\uD801\uDC37\uD852\uDF62陈";
        System.out.println(str1.codePointCount(0, str1.length() - 1));
        System.out.println(str1.codePointAt(0));
        System.out.println(str1.charAt(3));
        System.out.println(str1.codePointBefore(4));
        System.out.println("陈".getBytes().length);
        int index = str1.indexOf(57186);
        System.out.println(index);
        System.out.println(str1.indexOf("陈"));

        String s0 = "zhuo";
        String s1 = "li";
        String s2 = "zhuoli";
        String s3 = (new StringBuilder(s0).append(s1)).toString().intern();
        System.out.println(s2 == s3);

        String delimiter = ",";
        String delimiterResult = String.join(delimiter, Lists.newArrayList("this", "is", "java"));
        System.out.println(delimiterResult);

        System.out.print("UTF-8: ");
        printHexString("中".getBytes());

        System.out.print("UTF-16: ");
        printHexString("中".getBytes(StandardCharsets.UTF_16BE));

    }

    private static void printHexString(byte[] b) {
        StringBuilder hex = new StringBuilder();
        for (int i = 0; i < b.length; i++) {
             hex.append(Strings.padStart(Integer.toHexString(b[i] & 0xFF), 2, '0'));
        }
        String result = "\\u" + hex.toString().toUpperCase();
        System.out.println(result);
    }
}



