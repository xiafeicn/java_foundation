package com.zhuoli.service.thinking.java.clazz.protected0.other;

import com.zhuoli.service.thinking.java.clazz.protected0.ProtectedClass;
import org.junit.Test;

/**
 * @Author: zhuoli
 * @Date: 2018/10/18 14:51
 * @Description:
 */
public class ProtectedClassTest3Sub extends ProtectedClass {

    @Test
    public void test(){
        //父类protected方法子类可见，无论子类是不是与父类在同一个包下
        print();

        //子类与父类不在同一个包下，子类实例可以访问其从基类继承而来的protected方法
        ProtectedClassTest3Sub protectedClassTest3Sub = new ProtectedClassTest3Sub();
        protectedClassTest3Sub.print();

        //子类与父类不在同一个包下，则父类实例不能访问父类的protected方法，以下编译报错
        /*ProtectedClass protectedClass = new ProtectedClass();
        protectedClass.print();*/

        //注意protected内部类的继承规则跟protected成员方法略有不同，protected内部类严格遵守同包和子类可见条件，非同包即使时子类也不可见，以下报错
        /*InnerProtectedClass innerProtectedClass1 = new InnerProtectedClass();*/

        //这种方式也不行，因为不同包
        /*ProtectedClassTest3Sub protectedClassTest3Sub1 = new ProtectedClassTest3Sub();
        ProtectedClassTest3Sub.InnerProtectedClass innerProtectedClass2 = protectedClassTest3Sub1.new InnerProtectedClass();*/
    }
}
