package com.zhuoli.service.thinking.java.clazz.abstract_class;

/**
 * @Author: zhuoli
 * @Date: 2018/10/22 17:49
 * @Description:
 */
public class Base extends AbstractAdder {
    private static final int MAX_NUM = 1000;
    private int[] arr = new int[MAX_NUM];
    private int count;

    public void add(int number) {
        if (count < MAX_NUM) {
            arr[count++] = number;
        }
    }

    public int get(int index) {
        return arr[index];
    }

    public static void main(String[] args) {
        ArrayDemoInterface arrayDemoInterface = new Base();
        arrayDemoInterface.add(1);

        arrayDemoInterface.addAll(new int[]{4, 5, 6});

        System.out.println(arrayDemoInterface.get(1));
    }
}
