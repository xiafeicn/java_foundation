package com.zhuoli.service.thinking.java.clazz.inner_class.static_inner;

/**
 * @Author: zhuoli
 * @Date: 2018/10/22 21:56
 * @Description:
 */
public class Outer {
    private static int shared = 100;

    public static class StaticInner {
        public void innerMethod() {
            System.out.println("inner " + shared);
        }
    }

    public void useStaticInnerClass() {
        StaticInner si = new StaticInner();
        si.innerMethod();
    }

    public static void main(String[] args) {
        Outer.StaticInner staticInner = new Outer.StaticInner();
        staticInner.innerMethod();
    }
}
