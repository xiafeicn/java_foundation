package com.zhuoli.service.thinking.java.clazz.protected0;

import lombok.Getter;
import lombok.Setter;
import org.junit.Test;

/**
 * @Author: zhuoli
 * @Date: 2018/10/18 14:41
 * @Description:
 */
@Getter
@Setter
public class ProtectedClass {
    private String name;

    private Integer age;

    protected void print() {
        System.out.println("protected method");
    }

    @Getter
    @Setter
    protected class InnerProtectedClass {
        private String innerName;
    }

    @Test
    public void test() {
        //protected内部类在本类中可见
        InnerProtectedClass innerProtectedClass = new InnerProtectedClass();
        innerProtectedClass.setInnerName("k8s");
        System.out.println(innerProtectedClass.getInnerName());
    }
}
