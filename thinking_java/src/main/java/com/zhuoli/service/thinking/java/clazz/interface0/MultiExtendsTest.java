package com.zhuoli.service.thinking.java.clazz.interface0;

import lombok.AllArgsConstructor;

/**
 * @Author: zhuoli
 * @Date: 2018/10/22 14:02
 * @Description:
 */
@AllArgsConstructor
public class MultiExtendsTest implements TestInterface, MyComparable {
    private int distance;

    public int compareTo(Object other) {
        return this.distance - ((MultiExtendsTest) other).distance;
    }

    public static void main(String[] args) {
        TestInterface testInterface0 = new MultiExtendsTest(1);
        TestInterface testInterface1 = new MultiExtendsTest(2);
        System.out.println(((MultiExtendsTest) testInterface0).compareTo(testInterface1));
    }
}
