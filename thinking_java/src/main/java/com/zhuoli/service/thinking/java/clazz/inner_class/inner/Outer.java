package com.zhuoli.service.thinking.java.clazz.inner_class.inner;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author: zhuoli
 * @Date: 2018/10/23 12:19
 * @Description:
 */
@Getter
@Setter
public class Outer {

    private int a = 100;

    public class Inner {

        public void innerMethod() {
            System.out.println("outer a " + a);
            Outer.this.action();
        }
    }

    private void action() {
        System.out.println("action");
    }

    public void useInnerClass() {
        Inner inner = new Inner();
        inner.innerMethod();
    }

    public static void main(String[] args) {
        Outer outer = new Outer();
        outer.setA(99);

        Outer.Inner inner = outer.new Inner();
        inner.innerMethod();
    }
}

