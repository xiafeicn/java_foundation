package com.zhuoli.service.thinking.java.clazz.default0.other;

import org.junit.Test;

/**
 * @Author: zhuoli
 * @Date: 2018/10/18 22:00
 * @Description:
 */
public class OtherPackageTest {

    @Test
    public void test() {
        /*DefaultClass是包访问权限，在不同包下不可见，以下编译报错*/
        //DefaultClass defaultClass = new DefaultClass();
    }
}
