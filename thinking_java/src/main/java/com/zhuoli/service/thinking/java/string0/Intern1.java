package com.zhuoli.service.thinking.java.string0;

/**
 * @Author: zhuoli
 * @Date: 2018/10/31 20:50
 * @Description:
 */
public class Intern1 {
    public static void main(String[] args){
        String s1 = "123";
        String s2 = "456";
        String s3 = s1 + s2;
    }
}
