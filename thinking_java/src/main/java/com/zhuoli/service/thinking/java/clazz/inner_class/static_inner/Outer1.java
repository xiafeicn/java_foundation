package com.zhuoli.service.thinking.java.clazz.inner_class.static_inner;

/**
 * @Author: zhuoli
 * @Date: 2018/10/23 11:43
 * @Description:
 */
public class Outer1 {
    static {
        System.out.println("load outer class...");
    }

    //静态内部类
    static class StaticInner {
        static {
            System.out.println("load static inner class...");
        }

        static void staticInnerMethod() {
            System.out.println("static inner method...");
        }
    }

    public static void main(String[] args) {
        Outer1 outer = new Outer1();
        System.out.println("======================");
        //Outer1.StaticInner.staticInnerMethod();
        Outer1.StaticInner staticInner = new StaticInner();
    }
}