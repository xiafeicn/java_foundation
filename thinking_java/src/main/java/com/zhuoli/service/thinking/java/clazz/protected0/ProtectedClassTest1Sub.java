package com.zhuoli.service.thinking.java.clazz.protected0;

import org.junit.Test;

/**
 * @Author: zhuoli
 * @Date: 2018/10/18 14:51
 * @Description:
 */
public class ProtectedClassTest1Sub extends ProtectedClass {

    @Test
    public void test(){
        //父类protected方法子类可见
        print();

        //print来自父类ProtectedClass，访问权限时父类包和子类对象，当前两个条件都满足
        ProtectedClassTest1Sub protectedClassTest1Sub = new ProtectedClassTest1Sub();
        protectedClassTest1Sub.print();

        //print来自父类ProtectedClass，访问权限时父类包和子类对象，满足同包条件
        ProtectedClass protectedClass = new ProtectedClass();
        protectedClass.print();
    }
}
