package com.zhuoli.service.thinking.java.static0.static_block;

/**
 * @Author: zhuoli
 * @Date: 2018/10/24 10:53
 * @Description:
 */
public class Base {
    public static Integer baseStaticVar = 10;
    public Integer baseNonStaticVar = 10;

    static {
        System.out.println("========父类静态代码块开始========");
        System.out.println("before base static block, baseStaticVar = " + baseStaticVar);
        baseStaticVar = 9;
        System.out.println("base static block change baseStaticVar, baseStaticVar = " + baseStaticVar);
        System.out.println("========父类静态代码块结束========");
    }

    {
        System.out.println("========父类非静态代码块开始========");
        System.out.println("before base block, baseNonStaticVar = " + baseNonStaticVar);
        baseNonStaticVar = 9;
        System.out.println("base block change baseNonStaticVar, baseNonStaticVar = " + baseNonStaticVar);
        System.out.println("========父类非静态代码块结束========");
    }

    public Base() {
        System.out.println("========父类构造函数开始========");
        System.out.println("base constructor called");
        System.out.println("========父类构造函数结束========");
    }
}
