package com.zhuoli.service.thinking.java.clazz.anonymous_class;

/**
 * @Author: zhuoli
 * @Date: 2018/10/23 21:32
 * @Description:
 */
public class Test {
    public static void main(String[] args) {
        Person p = new Person() {
            public void eat() {
                System.out.println("eat work meal!");
            }
        };
        p.eat();
    }
}
