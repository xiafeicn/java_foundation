package com.zhuoli.service.thinking.java.exception;

import static java.lang.System.exit;

/**
 * @Author: zhuoli
 * @Date: 2018/10/31 10:10
 * @Description:
 */
public class Test {
    public static void main(String[] args) {
        System.out.println("main");
        try {
            System.out.println("try");
            exit(0);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            System.out.println("finally");
        }
    }
}


