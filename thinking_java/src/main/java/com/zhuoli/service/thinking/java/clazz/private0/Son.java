package com.zhuoli.service.thinking.java.clazz.private0;

/**
 * @Author: zhuoli
 * @Date: 2018/10/18 20:53
 * @Description:
 */
public class Son {

    private class Father_1 extends Father{
        public int strong(){
            return super.strong() + 1;
        }
    }

    private class Mother_1 extends  Mother{
        public int kind(){
            return super.kind() - 2;
        }
    }

    public int getStrong(){
        return new Father_1().strong();
    }

    public int getKind(){
        return new Mother_1().kind();
    }

    public static void main(String[] args) {
        Son son = new Son();
        System.out.println("Son's Strong：" + son.getStrong());
        System.out.println("Son's kind：" + son.getKind());
    }
}
