package com.zhuoli.service.thinking.java.static0.static_variable;

/**
 * @Author: zhuoli
 * @Date: 2018/10/24 10:01
 * @Description:
 */
public class StaticVariables {
    public static Integer staticInt = 0;
    public Integer nonStaticInt = 0;

    public static void main(String[] args) {
        StaticVariables var = new StaticVariables();
        System.out.println("var.staticInt: " + var.staticInt);
        System.out.println("StaticVariables.staticInt: " + StaticVariables.staticInt);
        System.out.println("var.nonStaticInt: " + var.nonStaticInt);
        System.out.println("静态变量与非静态变量加1");
        var.staticInt++;
        var.nonStaticInt++;
        StaticVariables var1 = new StaticVariables();
        System.out.println("var1.staticInt: " + var1.staticInt);
        System.out.println("StaticVariables.staticInt: " + StaticVariables.staticInt);
        System.out.println("var1.nonStaticInt: " + var1.nonStaticInt);
    }

}
