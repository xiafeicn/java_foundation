package com.zhuoli.service.thinking.java.clazz.interface0;

import java.util.Arrays;

/**
 * @Author: zhuoli
 * @Date: 2018/10/22 12:45
 * @Description:
 */
public class CompareUtils {

    /**
     * 获取最大值
     */
    public static Object getMax(MyComparable[] comparables) {
        if (comparables == null || comparables.length == 0) {
            return null;
        }
        MyComparable max = comparables[0];
        for (int i = 1; i < comparables.length; i++) {
            if (max.compareTo(comparables[i]) < 0) {
                max = comparables[i];
            }
        }
        return max;
    }

    /**
     * 升序排序
     */
    public static void sort(MyComparable[] comparables) {
        for (int i = 0; i < comparables.length; i++) {
            int min = i;
            for (int j = i + 1; j < comparables.length; j++) {
                if (comparables[j].compareTo(comparables[min]) < 0) {
                    min = j;
                }
            }
            if (min != i) {
                MyComparable temp = comparables[i];
                comparables[i] = comparables[min];
                comparables[min] = temp;
            }
        }
    }

    public static void main(String[] args) {
        Point[] points = new Point[]{
                new Point(2, 3),
                new Point(3, 4),
                new Point(1, 2)
        };
        System.out.println("max: " + CompareUtils.getMax(points));
        CompareUtils.sort(points);
        System.out.println("sort: " + Arrays.toString(points));
    }
}
