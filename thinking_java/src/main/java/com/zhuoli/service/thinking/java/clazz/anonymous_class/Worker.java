package com.zhuoli.service.thinking.java.clazz.anonymous_class;

/**
 * @Author: zhuoli
 * @Date: 2018/10/23 21:25
 * @Description:
 */
public class Worker extends Person{

    public void eat() {
        System.out.println("eat work meal!");
    }

    public static void main(String[] args) {
        Person p = new Worker();
        p.eat();
    }
}
