package com.zhuoli.service.thinking.java.clazz.protected0.other;

import com.zhuoli.service.thinking.java.clazz.protected0.ProtectedClass;
import org.junit.Test;

/**
 * @Author: zhuoli
 * @Date: 2018/10/18 14:54
 * @Description:
 */
public class ProtectedClassTest2 {

    @Test
    public void test(){
        ProtectedClass protectedClass = new ProtectedClass();

        //此处编译报错，因为InnerProtectedClass为同包和子类访问权限
       /* ProtectedClass.InnerProtectedClass innerProtectedClass = protectedClass.new InnerProtectedClass();
        innerProtectedClass.setInnerName("inner class");

        System.out.println(innerProtectedClass.getInnerName());*/
    }
}
