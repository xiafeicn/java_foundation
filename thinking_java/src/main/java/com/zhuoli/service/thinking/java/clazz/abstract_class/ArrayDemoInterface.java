package com.zhuoli.service.thinking.java.clazz.abstract_class;

/**
 * @Author: zhuoli
 * @Date: 2018/10/22 15:53
 * @Description:
 */
public interface ArrayDemoInterface {

    void add(int number);

    void addAll(int[] numbers);

    int get(int index);
}
