package com.zhuoli.service.thinking.java.clazz.interface0;

/**
 * @Author: zhuoli
 * @Date: 2018/10/22 12:39
 * @Description:
 */
public interface MyComparable {
    int compareTo(Object other);
}
