package com.zhuoli.service.thinking.java.static0.static_block;

/**
 * @Author: zhuoli
 * @Date: 2018/10/24 10:58
 * @Description:
 */
public class SubClass extends Base {
    public static Integer subClassStaticVar = 10;
    public Integer subClassNonStaticVar = 10;

    static {
        System.out.println("========子类静态代码块开始========");
        System.out.println("before SubClass static block, subClassStaticVar = " + subClassStaticVar);
        subClassStaticVar = 9;
        System.out.println("base SubClass block change subClassStaticVar, subClassStaticVar = " + subClassStaticVar);
        System.out.println("========子类静态代码块结束========");
    }

    {
        System.out.println("========子类非静态代码块开始========");
        System.out.println("before SubClass block, subClassNonStaticVar = " + subClassNonStaticVar);
        subClassNonStaticVar = 9;
        System.out.println("SubClass block change subClassNonStaticVar, subClassNonStaticVar = " + subClassNonStaticVar);
        System.out.println("========子类非静态代码块结束========");
    }

    public SubClass() {
        System.out.println("========子类构造函数开始========");
        System.out.println("base constructor called");
        System.out.println("========子类构造函数结束========");
    }

    public static void main(String[] args) {
        SubClass subClass = new SubClass();
    }
}
