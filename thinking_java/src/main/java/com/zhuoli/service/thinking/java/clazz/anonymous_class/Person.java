package com.zhuoli.service.thinking.java.clazz.anonymous_class;

/**
 * @Author: zhuoli
 * @Date: 2018/10/23 21:23
 * @Description:
 */
public abstract class Person {
    public abstract void eat();
}
