package com.zhuoli.service.thinking.java.clazz.abstract_class;

/**
 * @Author: zhuoli
 * @Date: 2018/10/22 15:54
 * @Description:
 */
public abstract class AbstractAdder implements ArrayDemoInterface {

    public void addAll(int[] numbers) {
        for(int num : numbers){
            add(num);
        }
    }
}
