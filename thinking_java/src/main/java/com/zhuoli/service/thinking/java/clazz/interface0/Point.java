package com.zhuoli.service.thinking.java.clazz.interface0;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @Author: zhuoli
 * @Date: 2018/10/22 12:41
 * @Description:
 */
@Getter
@Setter
@AllArgsConstructor
public class Point implements MyComparable {

    private int x;
    private int y;

    public double distance() {
        return Math.sqrt(x * x + y * y);
    }

    public int compareTo(Object other) {
        if (!(other instanceof Point)) {
            throw new IllegalArgumentException();
        }
        Point otherPoint = (Point) other;
        double delta = distance() - otherPoint.distance();
        if (delta < 0) {
            return -1;
        } else if (delta > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public String toString() {
        return "(" + x + "," + y + ")";
    }

    public static void main(String[] args) {
        MyComparable p1 = new Point(2,3);
        MyComparable p2 = new Point(1,2);
        System.out.println(p1.compareTo(p2));
    }
}
