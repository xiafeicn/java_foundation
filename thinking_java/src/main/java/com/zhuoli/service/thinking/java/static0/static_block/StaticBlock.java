package com.zhuoli.service.thinking.java.static0.static_block;

/**
 * @Author: zhuoli
 * @Date: 2018/10/24 10:41
 * @Description:
 */
public class StaticBlock {
    public static Integer staticVariable;

    static {
        StaticBlock.staticVariable = 10;
        System.out.println("static block is called");
        System.out.println("StaticBlock.staticVariable: " + staticVariable);
    }

    public static void main(String[] args) {

    }
}
