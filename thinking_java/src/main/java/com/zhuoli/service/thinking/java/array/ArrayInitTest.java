package com.zhuoli.service.thinking.java.array;

import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * @Author: zhuoli
 * @Date: 2018/10/17 20:48
 * @Description:
 */
public class ArrayInitTest {

    @Test
    public void initTest(){
        int[] arr0;

        int[] arr1 = new int[5];
        arr1[1] = 2;
        arr1[2] = 3;

        arr0 = arr1;

        int[] arr2 = new int[]{1, 2, 3};
        int[] arr3 = {1, 2, 3};

        Assert.assertEquals(5, arr0.length);
    }
}
