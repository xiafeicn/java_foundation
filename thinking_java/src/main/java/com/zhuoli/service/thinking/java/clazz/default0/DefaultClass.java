package com.zhuoli.service.thinking.java.clazz.default0;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author: zhuoli
 * @Date: 2018/10/18 21:51
 * @Description:
 */
@Getter
@Setter
class DefaultClass {
    private String name;

    private Integer age;

    void print() {
        System.out.println("protected method");
    }

    @Getter
    @Setter
    class InnerDefaultClass {
        private String innerName;
    }
}
