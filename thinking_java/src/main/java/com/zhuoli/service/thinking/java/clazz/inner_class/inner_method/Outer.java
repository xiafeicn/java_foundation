package com.zhuoli.service.thinking.java.clazz.inner_class.inner_method;

/**
 * @Author: zhuoli
 * @Date: 2018/10/23 16:13
 * @Description:
 */
public class Outer {
    private int a = 100;

    public void test(final int param) {
        final String str = "hello";
        class Inner {
            public void innerMethod() {
                System.out.println("outer a " + a);
                System.out.println("param " + param);
                System.out.println("local var " + str);
                a = 20;
            }
        }
        Inner inner = new Inner();
        inner.innerMethod();
        System.out.println("==============");
        System.out.println(a);
    }

    public static void main(String[] args) {
        Outer outer = new Outer();
        outer.test(999);
        System.out.println(outer.a);
    }
}