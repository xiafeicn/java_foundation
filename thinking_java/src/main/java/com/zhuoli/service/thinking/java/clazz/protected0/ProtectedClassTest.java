package com.zhuoli.service.thinking.java.clazz.protected0;

import org.junit.Test;

/**
 * @Author: zhuoli
 * @Date: 2018/10/18 14:44
 * @Description:
 */
public class ProtectedClassTest {
    private ProtectedClass protectedClass = new ProtectedClass();

    //protected类, 访问权限时同包和子类，当前ProtectedClassTest满足同包条件
    //构建时注意必须先构建父类实例protectedClass，不能使用new ProtectedClass.InnerProtectedClass()，否则会报not an enclosing class异常
    private ProtectedClass.InnerProtectedClass innerProtectedClass = protectedClass.new InnerProtectedClass();

    @Test
    public void test(){
        innerProtectedClass.setInnerName("inner class");
        System.out.println(innerProtectedClass.getInnerName());

        //包访问权限,父类实例可以在同包下调用自己的protected方法
        protectedClass.print();
    }
}
