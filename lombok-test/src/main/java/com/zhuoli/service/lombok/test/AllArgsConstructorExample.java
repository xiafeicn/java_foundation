package com.zhuoli.service.lombok.test;

import lombok.AllArgsConstructor;

/**
 * @Author: zhuoli
 * @Date: 2018/7/25 10:34
 * @Description:
 */
@AllArgsConstructor
public class AllArgsConstructorExample {
    private Integer x;

    private Integer y;

    private static Integer z;

    private final Integer z1;

    private final Integer z2 = 2;

    private static final Integer z3 = 1;
}
