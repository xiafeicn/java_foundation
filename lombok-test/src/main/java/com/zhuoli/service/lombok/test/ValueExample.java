package com.zhuoli.service.lombok.test;

import lombok.AccessLevel;
import lombok.ToString;
import lombok.Value;
import lombok.experimental.NonFinal;
import lombok.experimental.Wither;

/**
 * @Author: zhuoli
 * @Date: 2018/7/24 21:09
 * @Description:
 */
@Value
public class ValueExample {
    private String name;

    @Wither(AccessLevel.PACKAGE)
    @NonFinal
    private int age;

    private double score;
    protected String[] tags;

    @ToString
    @Value(staticConstructor="of")
    public static class Exercise<T> {
        String name;
        T value;
    }
}
