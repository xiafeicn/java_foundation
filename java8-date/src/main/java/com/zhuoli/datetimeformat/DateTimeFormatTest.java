package com.zhuoli.datetimeformat;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Author: zhuoli
 * Description:
 * Date: 下午1:10 2018/6/14
 */
public class DateTimeFormatTest {
    public void dateTimeFormatTest(){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy 年 MM 月 dd 日 HH 时 mm 分 ss 秒");

        ZonedDateTime zonedDateTime = Instant.ofEpochSecond(1528905600).atZone(ZoneId.of("Asia/Shanghai"));
        /*输出：2018 年 06 月 14 日 00 时 00 分 00 秒*/
        System.out.println(formatter.format(zonedDateTime));

        zonedDateTime = Instant.ofEpochSecond(1528905600).atZone(ZoneId.of("Asia/Tokyo"));
        /*输出：2018 年 06 月 14 日 01 时 00 分 00 秒*/
        System.out.println(formatter.format(zonedDateTime));

        LocalDateTime localDateTime = LocalDateTime.parse("2018 年 06 月 14 日 01 时 00 分 00 秒", formatter);
        /*输出：2018-06-14T01:00*/
        System.out.println(localDateTime);
    }

    public static void main(String[] args){
        DateTimeFormatTest dateTimeFormatTest = new DateTimeFormatTest();
        dateTimeFormatTest.dateTimeFormatTest();
    }
}
