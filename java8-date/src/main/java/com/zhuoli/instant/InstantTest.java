package com.zhuoli.instant;

import com.sun.corba.se.impl.protocol.INSServerRequestDispatcher;

import java.time.Instant;

/**
 * Author: zhuoli
 * Description:
 * Date: 下午12:18 2018/6/14
 */
public class InstantTest {
    public void instantTest(){
        Instant now = Instant.now();
        /*输出：2018-06-14T04:21:35.411Z*/
        System.out.println(now);

        Instant instant = Instant.ofEpochSecond(1528905600L);
        /*输出：2018-06-13T16:00:00Z*/
        System.out.println(instant);

        instant = Instant.ofEpochMilli(1528905600000L);
        /*输出：2018-06-13T16:00:00Z*/
        System.out.println(instant);

        instant = Instant.ofEpochSecond(1528905600L, 999);
        /*2018-06-13T16:00:00.000000999Z*/
        System.out.println(instant);
    }

    public static void main(String[] args){
        InstantTest instantTest = new InstantTest();
        instantTest.instantTest();
    }
}
