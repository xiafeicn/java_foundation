package com.zhuoli.simpledateformat;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Author: zhuoli
 * Description:
 * Date: 上午11:21 2018/6/14
 */
public class SimpleDateFormatTest {

    public static void main(String[] args) {
        try {
            SimpleDateFormatTest simpleDateFormatTest = new SimpleDateFormatTest();
            simpleDateFormatTest.simpleDateFormatTest();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void simpleDateFormatTest() throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        DateFormat dateFormatWithZone = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss XXX");

        /*格式化*/
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(0));
        /*输出：1970-01-01T08:00:00 +08:00*/
        System.out.println(dateFormatWithZone.format(calendar.getTime()));
        /*输出：1970-01-01T08:00:00*/
        System.out.println(dateFormat.format(calendar.getTime()));

        /*解析*/
        String timeWithZone = "2017-11-30T00:00:00 +00:00";
        /*转化为date对象*/
        Date dateWithZone = dateFormatWithZone.parse(timeWithZone);
        /*转化为String输出，输出：2017-11-30T08:00:00 +08:00*/
        System.out.println(dateFormatWithZone.format(dateWithZone));

        String time = "2017-11-30T00:00:00";
        /*转化为date对象*/
        Date date = dateFormat.parse(time);
        /*转化为String输出，输出：2017-11-30T08:00:00*/
        System.out.println(dateFormat.format(date));
    }
}
