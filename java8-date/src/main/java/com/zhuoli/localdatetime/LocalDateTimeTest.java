package com.zhuoli.localdatetime;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * Author: zhuoli
 * Description:
 * Date: 下午12:33 2018/6/14
 */
public class LocalDateTimeTest {
    public void localDateTimeTest(){
        LocalDateTime now = LocalDateTime.now();
        System.out.println(now);

        /*设置到分*/
        LocalDateTime dateTime = LocalDateTime.of(2018,6,14,0,0);
        /*输出：2018-06-14T00:00*/
        System.out.println(dateTime);

        /*设置毫秒*/
        dateTime = LocalDateTime.of(2018,6,14,0,0,30);
        /*输出：2018-06-14T00:00:30*/
        System.out.println(dateTime);

        /*设置到纳秒*/
        dateTime = LocalDateTime.of(2018,6,14,0,0,30, 999);
        /*输出：2018-06-14T00:00:30.000000999*/
        System.out.println(dateTime);

        /*LocalDate & LocalTime -> LocalDateTime*/
        LocalDate date = LocalDate.of(2018,6,14);
        LocalTime time = LocalTime.of(1, 0, 0, 30);

        dateTime =  date.atTime(time);
        /*输出：2018-06-14T01:00:00.000000030*/
        System.out.println(dateTime);

        dateTime = date.atTime(1,59,59);
        /*输出：2018-06-14T01:59:59*/
        System.out.println(dateTime);

        dateTime = date.atStartOfDay();
        /*输出：2018-06-14T00:00*/
        System.out.println(dateTime);

        dateTime = time.atDate(date);
        /*输出：2018-06-14T01:00:00.000000030*/
        System.out.println(dateTime);


        /*LocalDateTime -> LocalDate & LocalTime*/
        dateTime = LocalDateTime.of(2018,6,14,0,0,30, 999);

        date = dateTime.toLocalDate();
        /*输出：2018-06-14*/
        System.out.println(date);

        time = dateTime.toLocalTime();
        /*输出：00:00:30.000000999*/
        System.out.println(time);
    }

    public static void main(String[] args){
        LocalDateTimeTest localDateTimeTest = new LocalDateTimeTest();
        localDateTimeTest.localDateTimeTest();
    }
}
