package com.zhuoli.unixtime;

import java.util.Calendar;

/**
 * Author: zhuoli
 * Description:
 * Date: 上午7:54 2018/6/14
 */
public class DateUtil {

    public static void main(String[] args) {
        DateUtil dateUtil = new DateUtil();

        /*设置日期与时间 2018-06-14T00:00:00*/
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2017);
        /*Calendar类月份是0～11*/
        calendar.set(Calendar.MONTH, 5);
        calendar.set(Calendar.DATE, 14);
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        System.out.println(dateUtil.is0Clock(calendar.getTimeInMillis() / 1000));
    }

    public Boolean is0Clock(Long unixTimeStamp) {
        return (unixTimeStamp + 8 * 3600) % 86400 == 0;
    }
}

