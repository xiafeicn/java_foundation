package com.zhuoli.zoneddatetime;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

/**
 * Author: zhuoli
 * Description:
 * Date: 下午12:53 2018/6/14
 */
public class ZonedDateTimeTest {
    public void zonedDateTimeTest(){
        LocalDateTime localDateTime = LocalDateTime.of(2018, 6, 14,0,0,0);
        /*localDateTime设置为2018-06-14 00:00:00*/
        System.out.println(localDateTime);

        /*localDateTime的东八区本地时间*/
        ZonedDateTime zonedDateTime = localDateTime.atZone(ZoneId.of("Asia/Shanghai"));
        /*输出：2018-06-14T00:00+08:00[Asia/Shanghai]*/
        System.out.println(zonedDateTime);
        /*东八区本地时间转unix时间，输出：1528905600*/
        System.out.println(zonedDateTime.toInstant().getEpochSecond());

        /*由zonedDateTime获取相同的Unix时间下，东九区的zoneDateTime*/
        zonedDateTime = zonedDateTime.withZoneSameInstant(ZoneId.of("Asia/Tokyo"));
        /*输出：2018-06-14T01:00+09:00[Asia/Tokyo]*/
        System.out.println(zonedDateTime);
        /*东九区时间转unix时间，输出：1528905600*/
        System.out.println(zonedDateTime.toInstant().getEpochSecond());

        /*由东九区zoneDateTime，获取相同本地时间东八区的zoneDateTime*/
        zonedDateTime = zonedDateTime.withZoneSameLocal(ZoneId.of("Asia/Shanghai"));
        /*输出：2018-06-14T01:00+08:00[Asia/Shanghai]*/
        System.out.println(zonedDateTime);
        /*东八区unix时间，输出：1528909200*/
        System.out.println(zonedDateTime.toInstant().getEpochSecond());
    }

    public static void main(String[] args){
        ZonedDateTimeTest zonedDateTimeTest = new ZonedDateTimeTest();
        zonedDateTimeTest.zonedDateTimeTest();
    }
}
