package com.zhuoli.date;

import java.util.Date;

/**
 * Author: zhuoli
 * Description:
 * Date: 上午10:21 2018/6/14
 */
public class DateTest {
    public void dateTest(){
        Date date0 = new Date();
        System.out.println(String.format("%d - %s", date0.getTime(), date0.toString()));

        Date date1 = new Date(1528943069 * 1000L);
        System.out.println(String.format("%d - %s", date1.getTime(), date1.toString()));

        date1.setTime(0L);
        System.out.println(String.format("%d - %s", date1.getTime(), date1.toString()));
    }

    public static void main(String[] args){
        DateTest dateTest = new DateTest();
        dateTest.dateTest();
    }
}
