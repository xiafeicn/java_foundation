package com.zhuoli.guava.test.java8.sort.list;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @Author: zhuoli
 * @Date: 2018/7/31 10:19
 * @Description:
 */
@Getter
@Setter
@AllArgsConstructor
@ToString
public class Student {
    private Long id;

    private String name;

    private Integer score;
}
